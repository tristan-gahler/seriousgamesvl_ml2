﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour
{
    public CoinSpawner coinSpawner; //the coinspawner with the delegate when a coin is destroyed
    public Text display; //the display to display the score
    int score; //the actual score of the player

    void Start()
    {
        if(!this.coinSpawner)
            this.coinSpawner = FindObjectOfType<CoinSpawner>();
        this.score = 0;
        this.coinSpawner.onCoinDestroy += this.OnCoinDestroy; //subscribe to coin destroy delegate
        this.display.text = this.score.ToString();
    }

    //alter the score depenting on whether it was the correct coin or not
    void OnCoinDestroy(bool correctCoin){

        if(correctCoin){
            this.score++;
        }else{
            this.score--;
        }

        if(this.score < 0)
            this.score = 0;

        this.display.text = this.score.ToString();//update the dispaly
    }

}
