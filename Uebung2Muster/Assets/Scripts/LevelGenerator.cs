﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;

public class LevelGenerator : MonoBehaviour{
    string streamingAssets; //will be our saved path
    public string levelToLoad; //the level we want to load
    public List<Level> levels;
    //---------------- prefabs-----------------------
    public Wall wall;
    public Coin coin;
    public Floor floor;
    public Player player;
    public CoinSpawner coinSpawner;
    public int iterator;
    //-------------------------------------------------

    void Awake(){
        this.levels = new List<Level>();
        this.streamingAssets = Application.streamingAssetsPath + "/JsonTec"; //declare path

        this.coinSpawner = Instantiate(this.coinSpawner); //instantiate the prefab and save it to the same field

        //read all files in folder
        string[] files = Directory.GetFiles(this.streamingAssets);
        //filter the .meta files
        files.Where(f => !f.Contains(".meta")).ToList().ForEach(l => {
            StreamReader reader = new StreamReader(l);
            //add Level to List
            this.levels.Add(JsonUtility.FromJson<Level>(reader.ReadToEnd()));
            reader.Close();
        });

        this.iterator = 0;
        this.InitNextLevel(false);

 
    }

    public void InitNextLevel(bool levelToDestroy){
        if(levelToDestroy)
            this.DestroyCurrentLevel();
        
        Level level = this.levels[this.iterator];

        //give our coinSpawner the loaded floors (we need it to spawn anything)
        this.coinSpawner.floors = InstantiateObjects(this.floor.gameObject, level.floors).ConvertAll(f => f.transform).ToArray();

        //get all other Object
        InstantiateObjects(this.wall.gameObject, level.walls);
        
        InstantiateObjects(this.player.gameObject, new string[]{level.player});  

        //reset iterator if last level
        if(this.iterator >= this.levels.Count - 1){
            this.iterator = 0;
        //inc else
        }else{
            this.iterator++;
        }
    }

    void DestroyCurrentLevel(){
        //Find all relevant gameObjects, destroy them
        foreach(Wall w in FindObjectsOfType<Wall>()){
            Destroy(w.gameObject);
        }
        foreach(Floor f in FindObjectsOfType<Floor>()){
            Destroy(f.gameObject);
        }
        Destroy(FindObjectOfType<Player>().gameObject);
    }

    List<GameObject> InstantiateObjects(GameObject prefab, string[] jsonStrings){
        List<GameObject> gameObjects = new List<GameObject>();
        //convert each Object and instantiate the given prefab to it
        foreach(string s in jsonStrings){
            ObjectData data = JsonUtility.FromJson<ObjectData>(s);
            GameObject go = Instantiate(prefab, data.position, Quaternion.identity);
            go.transform.localScale = data.scale;
            gameObjects.Add(go);
        }
        //so we can easily access the instantiated gameObjects, we return them
        return gameObjects;
    }
}



