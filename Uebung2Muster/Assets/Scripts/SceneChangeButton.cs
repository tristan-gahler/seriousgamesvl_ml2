﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneChangeButton : MonoBehaviour
{
    public string destinationScene;
    void Start(){
        GetComponent<Button>().onClick.AddListener(this.ChangeScene);
    }

    void ChangeScene(){
        SceneChanger.instance.LoadScene(this.destinationScene);
    }
}
