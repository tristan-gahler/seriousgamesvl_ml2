﻿using UnityEngine;

//This class will spawn a new coin each time the old one is destroyed
public class CoinSpawner : MonoBehaviour
{
    #pragma warning disable 0649 //Disables Warning, that floor/coin are never assigned. We assign it via the editor
    public Transform[] floors; //to drag the floor cube into. The transform will be taken automatically
    [SerializeField] Coin coin; //the prefab with coin script attached to it
    Coin spawnedCoin; // private variable to hold track of current spawned coin
    public Operation Operation{get; private set;} //The current formula we try to solve
    public delegate void OnCoinDestroy(bool correctCoin); //c# delegate
    public OnCoinDestroy onCoinDestroy;
    LevelGenerator levelGenerator;

    //common pattern in c# for restraining a field (property in this case). _operationThreshold is only called from the getter/setter of operationThreshold
    //indicated by the "_".
    //usually in this case operationThreshold is public and _operationThreshold private
    float _operationThreshold = .5f;
    float OperationThreshold{get{
            return this._operationThreshold;} 
        set{ this._operationThreshold = value;
            if(this.OperationThreshold < 0) this.OperationThreshold = 0;
            if(this.OperationThreshold > 1) this.OperationThreshold = 1;
        }
    } //threshold for the operation to be chosen. 

    void Start()
    {
        this.levelGenerator = FindObjectOfType<LevelGenerator>();
        this.Respawn();
    }

    //when a coin gets hit, it has to call this method
    public void CoinHit(Coin coin){

        this.AlterThreshold(coin.correspondingNumber == this.Operation.Result());

        if(coin.correspondingNumber == this.Operation.Result()){
            if(this.onCoinDestroy != null)
                this.onCoinDestroy.Invoke(true); //invoke delegate and tell it was the correct coin

            //if theres a levelGenerator, we're in task 2, else in task 1
            if(this.levelGenerator)
                this.levelGenerator.InitNextLevel(true);
            this.Respawn();

        }else{
            if(this.onCoinDestroy != null)
                this.onCoinDestroy.Invoke(false); //invoke delegate and tell it was the wrong coin
        }
    }

    //check the operation and change it accordingly
    void AlterThreshold(bool correct){
        switch(this.Operation.OperationSymbol){
            case "+":
                this.OperationThreshold += correct ? .01f : -.01f;
                break;
            case "-":
                this.OperationThreshold += correct ? -.01f : .01f;
                break;
            default:
                break;
        }

        Debug.Log("current threshold = " + this.OperationThreshold);
    }

    void Respawn(){
        foreach(Coin coin in FindObjectsOfType<Coin>()){
            Destroy(coin.gameObject); //Destroy all remaining coins on the field;
        }

        if(Random.Range(0f, 1f) > this.OperationThreshold){ //Decide wich operation should be generated
            this.Operation = new Sum(Random.Range(0, 50), Random.Range(0, 50));
        }else{
            this.Operation = new Substract(Random.Range(0,50), Random.Range(0, 50));
        }

        this.Spawn(this.Operation.Result()); //spawn the correct coin
        for(int i = 0; i < 3; i++){ //spawn 3 maybe false coins (no check for doubled correct coins here)
            this.Spawn(Random.Range(-50, 100));
        }        
    }
    void Spawn(int correspondingNumber){
        int floorToChose = Random.Range(0, this.floors.Length);
        Transform floor = this.floors[floorToChose];
        float x = Random.Range(floor.position.x - floor.lossyScale.x,
                                floor.position.x + floor.lossyScale.x); //get range of floor to spawn new coin

        float y = Random.Range(floor.position.y - floor.lossyScale.y,
                                floor.position.y + floor.lossyScale.y);

        this.spawnedCoin = Instantiate(this.coin, new Vector2(x, y), Quaternion.identity).Init(correspondingNumber, this); //actually spawn a new coin
    }
}
