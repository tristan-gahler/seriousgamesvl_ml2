﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelSaver : MonoBehaviour
{
    public string level;
    string streamingAssets;
    void Start()
    {
        this.streamingAssets = Application.streamingAssetsPath + "/JsonTec";
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)){           
            //prepare parameter for Level-struct
            string[] walls = LevelObjectsToJson<Wall>();
            string[] floors = LevelObjectsToJson<Floor>();
            string player = LevelObjectsToJson<Player>()[0];

            //open a writer 
            StreamWriter writer = new StreamWriter(this.streamingAssets + "/Level" + this.level + ".json", false);
            //write jsonfile after creating the json from Levelstruct
            Level level = new Level(walls, floors, player);
            writer.Write(JsonUtility.ToJson(level));
            //never forget to close a writer ;)
            writer.Close();        
        }
        
    }

    //using a Generic here to avoid switchcases and boilerplate
    string[] LevelObjectsToJson<T>() where T : MonoBehaviour{
        T[] objects = FindObjectsOfType<T>(); //get all relevant objects
        string[] objectJsons = new string[objects.Length]; //prepare return value

        ObjectData data;
        //save each Objects relevant values to return value
        for(int i = 0; i < objects.Length; i++){
            data = new ObjectData(objects[i].transform.position, objects[i].transform.lossyScale);
            objectJsons[i] = JsonUtility.ToJson(data);
        }

        return objectJsons;
    }
}
