﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    //singleton pattern
    public static SceneChanger instance;
    void Start(){
        //Destroy gameObject if sceneChanger already exists. Important if we come back to this scene or have multiple scenes with sceneChanger
        if(!instance){
            DontDestroyOnLoad(this.gameObject);
            instance = this;
        }else{
            Destroy(this.gameObject);
        }
        
    }

    //Use SceneManagment to load scene by name
    //Buttons can only access public methods
    public void LoadScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }
}
